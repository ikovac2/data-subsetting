CREATE_SCHEMA = "DROP SCHEMA IF EXISTS {schema} CASCADE; \
CREATE SCHEMA {schema}; \
\
CREATE TABLE IF NOT EXISTS {schema}.tables ( \
    table_id int GENERATED ALWAYS AS IDENTITY PRIMARY KEY, \
    table_name varchar(255) NOT NULL \
); \
 \
CREATE TABLE IF NOT EXISTS {schema}.table_columns ( \
    column_id int GENERATED ALWAYS AS IDENTITY PRIMARY KEY, \
    table_id int, \
    column_name varchar(255), \
	CONSTRAINT FK_columns FOREIGN KEY (table_id) REFERENCES {schema}.tables (table_id) \
); \
\
CREATE TABLE IF NOT EXISTS {schema}.unique_keys ( \
    key_name varchar(255) NOT NULL PRIMARY KEY, \
    table_id int NOT NULL, \
	CONSTRAINT FK_table_id FOREIGN KEY (table_id) REFERENCES {schema}.tables (table_id) \
); \
\
CREATE TABLE IF NOT EXISTS {schema}.unique_columns( \
    key_name varchar(255), \
    column_id int UNIQUE, \
    CONSTRAINT primary_key_uc PRIMARY KEY (key_name, column_id), \
	CONSTRAINT FK_key_name FOREIGN KEY (key_name) REFERENCES {schema}.unique_keys (key_name), \
	CONSTRAINT FK_unique_keys FOREIGN KEY (column_id) REFERENCES {schema}.table_columns (column_id) \
); \
\
CREATE TABLE IF NOT EXISTS {schema}.foreign_keys ( \
    fk_name varchar(255) not null PRIMARY KEY, \
    pk_table_id int NOT NULL, \
    fk_table_id int NOT NULL, \
	CONSTRAINT FK_table_id_fk FOREIGN KEY (fk_table_id) REFERENCES {schema}.tables (table_id), \
	CONSTRAINT FK_table_id_pk FOREIGN KEY (pk_table_id) REFERENCES {schema}.tables (table_id) \
); \
\
CREATE TABLE IF NOT EXISTS {schema}.foreign_columns ( \
    fk_name varchar(255), \
    pk_column_id int, \
    fk_column_id int, \
    CONSTRAINT primary_key_fc PRIMARY KEY (fk_name, fk_column_id, pk_column_id), \
	CONSTRAINT FK_fk_id FOREIGN KEY (fk_name) REFERENCES {schema}.foreign_keys (fk_name), \
	CONSTRAINT FK_foreiqn_keys_pc FOREIGN KEY (pk_column_id) REFERENCES {schema}.table_columns (column_id), \
	CONSTRAINT FK_foreiqn_keys_fc FOREIGN KEY (fk_column_id) REFERENCES {schema}.table_columns (column_id) \
);"