class PrimaryKey:
    def __init__(self, name):
        self.name = name
        self.columns = []

class ForeignKey:
    def __init__(self, name):
        self.name = name
        self.pk_fk_columns = dict()
        self.fk_table = None
        self.pk_table = None

class Table:
    def __init__(self, name, id):
        self.name = name
        self.id = id
        self.primary_key = None
        self.foreign_keys = []
        self.columns = []

class Model:
    def __init__(self, tables, schema):
        self.tables = tables
        self.schema = schema
        self.connections = None

    def get_table(self, table_name):
        for table in self.tables:
            if table.name == table_name:
                return table
            
    def create_connections(self):
        self.connections = dict()

        for table in self.tables:
            self.connections[table.name] = set()
            for fk in table.foreign_keys:
                self.connections[table.name].add(fk.pk_table)
                if fk.pk_table.name not in self.connections:
                    self.connections[fk.pk_table.name] = set()
                    self.connections[fk.pk_table.name].add(table)
                else:
                    self.connections[fk.pk_table.name].add(table)

class Node:
    def __init__(self, path, table):
        self.path = path
        self.table = table