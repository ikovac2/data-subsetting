import psycopg2

def connect_to_database(host, database, user, password):
    return psycopg2.connect(host=host, database=database, user=user, password=password)

def execute_query(query, db):
    cur = db.cursor()
    cur.execute(query)

    return cur

def get_column_names(table, db, schema):
    query = "SELECT * FROM {}.{}".format(schema, table)
    cur = db.cursor()
    cur.execute(query)

    column_names = [desc[0] for desc in cur.description]

    cur.close()
    return column_names

def get_all_tables(db, schema):
    cursor = db.cursor()
    cursor.execute("SELECT table_name FROM INFORMATION_SCHEMA.tables WHERE table_schema = '{}';".format(schema))
    tables =  cursor.fetchall()
    cursor.close()
    return tables

def get_unique_keys(db, schema):
    cursor = db.cursor()
    cursor.execute("SELECT tc.table_name as table_name, \
		kc.column_name as column_name, \
		kc.constraint_name as constraint_name FROM \
	information_schema.table_constraints tc \
	JOIN information_schema.key_column_usage kc \
		ON tc.constraint_name = kc.constraint_name \
WHERE tc.constraint_type = 'PRIMARY KEY' AND kc.table_schema = '{}'".format(schema))
    tables =  cursor.fetchall()
    cursor.close()
    return tables

def get_foreign_keys(db, schema):
    cursor = db.cursor()
    cursor.execute("SELECT  kc1.table_name as fk_table, \
		kc1.column_name as fk_column, \
        kc2.table_name as pk_table,\
		kc2.column_name as pk_column, \
         rc.constraint_name as fk_name FROM \
	information_schema.referential_constraints rc \
	JOIN information_schema.key_column_usage kc1 \
		ON rc.constraint_name = kc1.constraint_name \
	JOIN information_schema.key_column_usage kc2 \
		ON rc.unique_constraint_name = kc2.constraint_name \
		AND kc1.ordinal_position = kc2.ordinal_position \
WHERE kc1.table_schema = '{}'".format(schema))
    tables =  cursor.fetchall()
    cursor.close()
    return tables
