from utils.models.DB_model import Node
from utils.PgAdmin_tool import execute_query

def get_paths(model, tables):
    paths = []
    for table in tables:
        table = table.strip()
        paths.extend(get_path(model, table, tables))

    return paths

def get_path(model, start, end):
    paths = []
    connections = model.connections
    start_table = model.get_table(start)

    start_path = []
    start_path.append(start_table)
    open = []

    open.append(Node(start_path, start_table))

    while len(open) != 0:
        n = open.pop()
        if end.issubset({x.name for x in n.path}):
            paths.append(n)
            continue
        
        for table in connections[n.table.name]:
            if table not in n.path:
                node = Node(n.path.copy(), table)
                node.path.append(table)
                open.append(node)

    return paths

def get_number_of_rows(db, table):
    sql = "SELECT COUNT(*) FROM {}".format(table)

    cursor = execute_query(sql, db)
    count = cursor.fetchall()[0][0]
    cursor.close()

    return count


def make_from_sql(path):
    sql_join = None
    prev_table = None

    for table in path:
        is_fk = False
        if sql_join is None:
            sql_join = "FROM " + table.name + " "
            prev_table = table
            continue
        sql_join += "JOIN " + table.name + " ON "

        for key in table.foreign_keys:
            if key.pk_table == prev_table:
                is_fk = True
                pk_table = key.pk_table.name
                fk_table = key.fk_table.name

                for column in key.pk_fk_columns:
                    sql_join += "{}.{} = {}.{} AND ".format(pk_table, column, fk_table, key.pk_fk_columns[column])

                sql_join = sql_join[:-4]
                break

        if not is_fk:
            for key in prev_table.foreign_keys:
                if key.pk_table == table:
                    pk_table = key.pk_table.name
                    fk_table = key.fk_table.name

                    for column in key.pk_fk_columns:
                        sql_join += "{}.{} = {}.{} AND ".format(pk_table, column, fk_table, key.pk_fk_columns[column])

                    sql_join = sql_join[:-4]
                    break
        
        prev_table = table

    return sql_join

def make_normal_sql_select(tables):
    return "SELECT * "

def make_stratified_perc_sql(sql_from, sql_where, attr, perc, tables, model):
    sql = "SELECT DISTINCT {} FROM ("
    sql += "SELECT {}.*"
    sql += ", row_number() over (partition by {} order by random()) rbr".format(attr)
    sql += ", ROUND(COUNT(*) over (partition by spol) * {}) cntsp ".format(perc)
    sql += sql_from
    sql += sql_where
    sql += " ) t WHERE rbr <= cntsp"

    sqls = []

    for table in tables:
        column_names = model.get_table(table).columns
        select = ""
        for name in column_names:
            select += "t." + name + ", "
        sqls.append((sql.format(select[:-2], table), table))

    return sqls

def make_stratified_num_sql(sql_from, sql_where, attr, count, tables, model):
    sql = "SELECT DISTINCT {} FROM ("
    sql += "SELECT {}.*"
    sql += ", row_number() over (partition by {} order by random()) rbr ".format(attr)
    sql += sql_from
    sql += sql_where
    sql += " ) t WHERE rbr <= {}".format(count)

    sqls = []

    for table in tables:
        column_names = model.get_table(table).columns
        select = ""
        for name in column_names:
            select += "t." + name + ", "
        sqls.append((sql.format(select[:-2], table), table))

    return sqls

def make_random_num_sql(sql_from, sql_where, count, tables, model):
    sql = "SELECT DISTINCT {} FROM ("
    sql += "SELECT {}.*"
    sql += ", random() as rnd "
    sql += sql_from
    sql += sql_where
    sql += " ORDER BY rnd LIMIT {}) t".format(count)

    sqls = []

    for table in tables:
        column_names = model.get_table(table).columns
        select = ""
        for name in column_names:
            select += "t." + name + ", "
        sqls.append((sql.format(select[:-2], table), table))

    return sqls

def make_random_perc_sql(db, sql_from, sql_where, table, perc, tables, model):
    count = int(get_number_of_rows(db, table) * perc)
    sql = "SELECT DISTINCT {} FROM ("
    sql += "SELECT {}.*"
    sql += ", random() as rnd "
    sql += sql_from
    sql += sql_where
    sql += " ORDER BY rnd LIMIT {}) t".format(str(count))

    sqls = []

    for table in tables:
        column_names = model.get_table(table).columns
        select = ""
        for name in column_names:
            select += "t." + name + ", "
        sqls.append((sql.format(select[:-2], table), table))

    return sqls

def make_where(predicate):
    return "WHERE " + predicate

def make_normal_sql(from_part, where_part, tables):
    sql = "SELECT DISTINCT {}.* "
    sql += from_part
    sql += where_part

    sqls = []

    for table in tables:
        sqls.append((sql.format(table), table))

    return sqls

def print_paths(paths):
    count = 1
    for node in paths:
        path = node.path
        line = str(count) + "  "
        for table in path:
            line += table.name + "->"
        print(line[:-2])
        count += 1

def export_tables(db, new_schema, sqls):
    sql = "CREATE SCHEMA {}; ".format(new_schema)

    for sqlq in sqls:
        sql += "CREATE TABLE {}.{} AS ".format(new_schema, sqlq[1])
        sql += sqlq[0] + "; "

    execute_query(sql, db)

    db.commit()

    return sql