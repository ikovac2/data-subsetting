from utils.PgAdmin_tool import execute_query
from datetime import datetime

def logger(db, start_schema, end_schema, tables, sql_subsetting):
    table_names = ""
    for table in tables:
        table_names += table + ", "
    table_names = table_names[:-2]
    curr_time = datetime.now()
    sql = "CREATE TABLE IF NOT EXISTS {}.logs (\
    start_scheme varchar(255), end_scheme varchar(255), date_time varchar(255), tables varchar(255), sql varchar(10000)\
    ); ".format(start_schema)

    sql += "INSERT INTO {}.logs (start_scheme, end_scheme, date_time, tables, sql) VALUES (\
    '{}', '{}', '{}', '{}', '{}'\
    )".format(start_schema, start_schema, end_schema, curr_time, table_names, sql_subsetting)

    execute_query(sql, db)

    db.commit()