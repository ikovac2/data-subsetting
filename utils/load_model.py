import utils.PgAdmin_tool as db_tool
import utils.models.DB_model as db_model

def get_tables(db, schema):
    sql = "SELECT * FROM {}.tables".format("metamodel_" + schema)
    cursor = db_tool.execute_query(sql, db)

    tables = cursor.fetchall()
    cursor.close()

    return tables

def get_primary_key(db, table_id, schema):
    sql = "SELECT * FROM {}.unique_keys WHERE table_id = ".format("metamodel_" + schema) + str(table_id)

    cursor = db_tool.execute_query(sql, db)

    primary_key = cursor.fetchall()
    cursor.close()

    return primary_key[0]

def get_primary_key_columns(db, primary_key, schema):
    sql = "SELECT column_name FROM {}.unique_columns uc \
    JOIN {}.table_columns dbc ON dbc.column_id = uc.column_id WHERE key_name = \'".format("metamodel_" + schema, "metamodel_" + schema) + primary_key + "\'"
    cursor = db_tool.execute_query(sql, db)

    pk_columns = cursor.fetchall()
    cursor.close()

    return pk_columns

def get_foreign_keys(db, table_id, schema):
    sql = "SELECT * FROM {}.foreign_keys WHERE fk_table_id = ".format("metamodel_" + schema) + str(table_id)

    cursor = db_tool.execute_query(sql, db)

    foreign_keys = cursor.fetchall()
    cursor.close()

    return foreign_keys

def get_foreign_key_columns(db, foreign_key, schema):
    sql = "SELECT * FROM {}.foreign_columns WHERE fk_name = '{}'".format("metamodel_" + schema, foreign_key)
    cursor = db_tool.execute_query(sql, db)

    foreign_columns = cursor.fetchall()
    cursor.close()

    return foreign_columns

def get_columns(db, table_id, schema):
    sql = "SELECT column_name FROM {}.table_columns WHERE table_id = ".format("metamodel_" + schema) + str(table_id)
    cursor = db_tool.execute_query(sql, db)

    columns = cursor.fetchall()
    cursor.close()

    return columns

def get_column_by_id(db, column_id, schema):
    sql = "SELECT column_name FROM {}.table_columns WHERE column_id = {}".format("metamodel_" + schema, str(column_id))
    cursor = db_tool.execute_query(sql, db)

    column_name = cursor.fetchall()
    cursor.close()

    return column_name[0][0]

def get_table_by_id(db, table_id, schema):
    sql = "SELECT table_name FROM {}.tables WHERE table_id = {}".format("metamodel_" + schema, str(table_id))
    cursor = db_tool.execute_query(sql, db)

    table_name = cursor.fetchall()
    cursor.close()

    return table_name[0][0]

def load_model(db, schema):
    model = db_model.Model([], schema)

    for table_id, table_name in get_tables(db, schema):
        table = db_model.Table(table_name, table_id)
        model.tables.append(table)

    for table in model.tables:
        table_id = table.id
        table_name = table.name
        pk_name, _ = get_primary_key(db, table_id, schema)
        pk_columns = get_primary_key_columns(db, pk_name, schema)
        columns = get_columns(db, table_id, schema)
        Primary_key = db_model.PrimaryKey(pk_name)
        
        for column in pk_columns:
            Primary_key.columns.append(column[0])

        table.primary_key = Primary_key

        for column in columns:
            table.columns.append(column[0])

        for foreign_key, pk_table_id, _ in get_foreign_keys(db, table_id, schema):
            key = db_model.ForeignKey(foreign_key)
            key.fk_table = table
            key.pk_table = model.get_table(get_table_by_id(db, pk_table_id, schema))

            for _, pk_column, fk_column in get_foreign_key_columns(db, foreign_key, schema):
                pk_column_name = get_column_by_id(db, pk_column, schema)
                fk_column_name = get_column_by_id(db, fk_column, schema)
                key.pk_fk_columns[pk_column_name] = fk_column_name

            table.foreign_keys.append(key)

    model.create_connections()

    return model