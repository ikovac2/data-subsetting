from utils.PgAdmin_tool import execute_query, get_all_tables, get_column_names, get_foreign_keys, connect_to_database, \
get_unique_keys
from utils.models.meta_model_baze import CREATE_SCHEMA
from collections import defaultdict

def create_model(db, schema):
    tmp = execute_query(CREATE_SCHEMA.format(schema="metamodel_" + schema), db)
    tmp.close()
    db.commit()

    tables = dict()
    columns = defaultdict()
    keys = set()

    for table in get_all_tables(db, schema):
        table = table[0]
        sql = "INSERT INTO {}.tables (table_name) VALUES ( \'".format("metamodel_" + schema)
        sql += table
        sql += "');"

        execute_query(sql, db)

        sql = "SELECT table_id FROM {}.tables WHERE table_name = \'".format("metamodel_" + schema)
        sql += table + "\'"
        a = execute_query(sql, db)
        id = a.fetchall()[0][0]
        a.close()

        tables[table] = id

        db.commit()

        columns[table] = dict()

        columns_n = get_column_names(table, db, schema)

        for name in columns_n:

            sql = "INSERT INTO {}.table_columns (table_id, column_name) VALUES ( ".format("metamodel_" + schema)
            sql += str(id) + ", \'" + name + "\');"
            execute_query(sql, db)

            db.commit()

            sql = "SELECT column_id from {}.table_columns WHERE table_id = ".format("metamodel_" + schema) + str(id)
            sql += " AND column_name = \'" + name + "\';"
            cursor = execute_query(sql, db)
            c_id = cursor.fetchall()[0][0]
            cursor.close()

            columns[table][name] = c_id

    keys = set()
    sql = ""
    for pk_table, pk_column, key_name in get_unique_keys(db, schema):
        if key_name not in keys:
            keys.add(key_name)

            sql += "INSERT INTO {}.unique_keys (key_name, table_id) VALUES ( \'".format("metamodel_" + schema)
            sql += key_name + "\', "
            sql += str(tables[pk_table])
            sql += ");"

        sql += "INSERT INTO {}.unique_columns (key_name, column_id) VALUES ( \'".format("metamodel_" + schema)
        sql += key_name + "\', " + str(columns[pk_table][pk_column]) + ");"
    execute_query(sql, db)

    db.commit()

    keys = set()
    sql = ""
    for fk_table, fk_column, pk_table, pk_column, key_name in get_foreign_keys(db, schema):
        if key_name not in keys:
            keys.add(key_name)

            sql += "INSERT INTO {}.foreign_keys (fk_name, pk_table_id, fk_table_id) VALUES ( \'".format("metamodel_" + schema)
            sql += key_name + "\', "
            sql += str(tables[pk_table]) + ", "
            sql += str(tables[fk_table])
            sql += ");"

        sql += "INSERT INTO {}.foreign_columns (fk_name, pk_column_id, fk_column_id) VALUES ( \'".format("metamodel_" + schema)
        sql += key_name + "\', " + str(columns[pk_table][pk_column]) + ", " + str(columns[fk_table][fk_column]) + ");"

    execute_query(sql, db)

    db.commit()
