import sys

from utils.create_model import create_model
from utils.load_model import load_model
from utils.subsetting_tool import *
from utils.PgAdmin_tool import connect_to_database
from utils.logger.logger import logger

def main():
    db = connect_to_database(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    print("Welcome to the subsetting tool!")
    schema = input("Please enter the name of your schema: ")
    create_model2 = input("Do you wish to chreate new model for your schema [y/n]: ")
    if create_model2 == "y":
        create_model(db, schema)

    tables = input("Enter desired tables separated by commas: ")
    tables = tables.strip().split(",")
    tables = {x.strip() for x in tables}

    model = load_model(db, schema)
    paths = get_paths(model, tables)
    print_paths(paths)

    index = int(input("Please choose number of path you want to use: "))

    from_part = make_from_sql(paths[index - 1].path)

    predicate = input("Plese enter WHERE conditions [table.column CONDITION AND/OR...]: ")
    if (predicate == ""):
        sql_where = ""
    else:
        sql_where = make_where(predicate)

    quantity_b = input("Would you like to set quantity of a sample [y/n]: ")
    if (quantity_b == "y"):
        stratify_b = input("Would you like stratified sample [y/n]: ")
        if (stratify_b == "y"):
            stratify_attr = input("Enter name of attribute [table_name.attribute]: ")
            int_b = input("Would you like to enter number of rows or percentage [r/p]: ")
            if (int_b == "p"):
                perc = input("Enter percentage: ")
                perc = float(perc) / 100
                sql = make_stratified_perc_sql(from_part, sql_where, stratify_attr, perc, tables, model)
            else:
                quantity = quantity = input("Input number of rows: ")
                quantity = int(quantity)
                sql = make_stratified_num_sql(from_part, sql_where, stratify_attr, quantity, tables, model)
        else:
            rows_perc = input("Would you like to input number of rows or percentage [r/p]: ")
            if (rows_perc == "r"):
                quantity = input("Input number of rows: ")
                quantity = int(quantity)
                sql = make_random_num_sql(from_part, sql_where, quantity, tables, model)
            else:
                table = input("For which table would you like to enter percentage [schema.table]: ")
                quantity = input("Enter percentage: ")
                quantity = int(quantity) * 1.0 / 100
                sql = make_random_perc_sql(db, from_part, sql_where, table, quantity, tables, model)
    else:
        sql = make_normal_sql(from_part, sql_where, tables)

    new_schema = input("Please enter new schema name for your export: ")
    
    final_sql = export_tables(db, new_schema, sql)

    logger(db, schema, new_schema, tables, final_sql)

main()